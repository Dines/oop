// ch01_06.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <locale>
//setlocale(LC_ALL,"Turkish");
using namespace std;
void f1(int a);//One argument
void f1(int a, int bl);//Two arguments
int main()
{
	setlocale(LC_ALL, "Turkish"); int x;
	f1(3, 7);
	f1(10);
	f1(30, 70);
	return 0;
}
//One argument
void f1(int a)
{
	cout << "one  parameter :" << a << endl;
}
//Two arguments
void f1(int a, int b)
{
	cout << "The first parameter :" << a << "    The second parameter :" << b << endl;
	cout << "product of a and b :" << a*b << endl;
}






